/* eslint-disable camelcase */
import { locationIconSize } from '../common'
import { LocationIconDef } from '../types'
import { RuinsIcon } from './types'

const generic = locationIconSize

export const ruinsIcons: Record<RuinsIcon, LocationIconDef> = {
  ruins_1: {
    height: generic.height,
    path: 'locations/ruins/1.png',
    opacity: 1,
    fontScale: generic.font
  },
  ruins_2: {
    height: generic.height,
    path: 'locations/ruins/2.png',
    opacity: 1,
    fontScale: generic.font
  },
  ruins_3: {
    height: generic.height,
    path: 'locations/ruins/3.png',
    opacity: 1,
    fontScale: generic.font
  },
  ruins_4: {
    height: generic.height,
    path: 'locations/ruins/4.png',
    opacity: 1,
    fontScale: generic.font
  },
  ruins_5: {
    height: generic.height,
    path: 'locations/ruins/5.png',
    opacity: 1,
    fontScale: generic.font
  },
  ruins_6: {
    height: generic.height,
    path: 'locations/ruins/6.png',
    opacity: 1,
    fontScale: generic.font
  }
}
