/* eslint-disable camelcase */
import { IconDef } from '../../../types'
import { TownIcon } from './types'

const height = 0.8

export const town__icons: Record<TownIcon, IconDef> = {
  town_1: {
    height,
    path: 'terrain/elements/towns/1.png',
    opacity: 1
  },
  town_2: {
    height,
    path: 'terrain/elements/towns/2.png',
    opacity: 1
  },
  town_3: {
    height,
    path: 'terrain/elements/towns/3.png',
    opacity: 1
  },
  town_4: {
    height,
    path: 'terrain/elements/towns/4.png',
    opacity: 1
  },
  town_5: {
    height,
    path: 'terrain/elements/towns/5.png',
    opacity: 1
  },
  town_6: {
    height,
    path: 'terrain/elements/towns/6.png',
    opacity: 1
  }
}
