/* eslint-disable camelcase */
import { IconDef } from '../../../types'
import { CityIcon } from './types'

const height = 1.2

export const city__icons: Record<CityIcon, IconDef> = {
  city_1: {
    height,
    path: 'terrain/elements/cities/1.png',
    opacity: 1
  },
  city_2: {
    height,
    path: 'terrain/elements/cities/2.png',
    opacity: 1
  },
  city_3: {
    height,
    path: 'terrain/elements/cities/3.png',
    opacity: 1
  }
}
