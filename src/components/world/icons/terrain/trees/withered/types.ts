export type withered_icon =
  | 'withered_1'
  | 'withered_2'
  | 'withered_3'
  | 'withered_4'
  | 'withered_5'
  | 'withered_6'
  | 'withered_7'
  | 'withered_8'
