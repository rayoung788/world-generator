/* eslint-disable camelcase */
import { IconDef } from '../../../types'
import { swamp_icon } from './types'

const base_height = 3

export const swamp__icons: Record<swamp_icon, IconDef> = {
  swamp_1: {
    height: base_height,
    path: 'terrain/trees/swamp/1/1.png',
    opacity: 0.6
  },
  swamp_2: {
    height: base_height,
    path: 'terrain/trees/swamp/1/2.png',
    opacity: 0.6
  },
  swamp_3: {
    height: base_height,
    path: 'terrain/trees/swamp/1/3.png',
    opacity: 0.6
  },
  swamp_4: {
    height: 2,
    path: 'terrain/trees/swamp/1/4.png',
    opacity: 0.6
  },
  swamp_5: {
    height: base_height,
    path: 'terrain/trees/swamp/1/5.png',
    opacity: 0.6
  },
  swamp_6: {
    height: base_height,
    path: 'terrain/trees/swamp/1/6.png',
    opacity: 0.6
  },
  swamp_7: {
    height: base_height,
    path: 'terrain/trees/swamp/1/7.png',
    opacity: 0.6
  },
  swamp_8: {
    height: base_height,
    path: 'terrain/trees/swamp/2/4.png',
    opacity: 0.6
  }
}
