import { DrawMapParams } from '../shapes/types'

export type DrawOceanParams = DrawMapParams & {
  month: number
}
