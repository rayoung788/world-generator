import { ReactNode } from 'react'

export interface CodexTitleProps {
  title: string
  subtitle: ReactNode
}
