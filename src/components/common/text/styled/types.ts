export type DetailedToolTipParams = {
  title: string
  subtitle: string
  content: { label: string; text: string }[]
}
