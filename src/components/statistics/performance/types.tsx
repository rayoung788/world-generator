import { Profiles } from '../../../models/utilities/performance/types'

export type PerformanceViewProps = {
  mode: keyof Profiles
}
