export const cssColors = {
  primary: '#58180d',
  blue: '#0d1456',
  purple: '#420d54',
  secondary: '#c9ad6a',
  plague: '#4d5e27',
  background: {
    map: '#e6edef',
    cards: '#f7f2e1',
    legend: '#faf5e9'
  },
  difficulty: {
    easy: '#0c4900',
    medium: '#bf762d',
    hard: '#630000',
    deadly: 'gray'
  },
  subtitle: '#8e8e8e',
  black: 'black'
}
