export type DegreeToPerimeterParams = {
  degree: number
  height: number
  width: number
}
