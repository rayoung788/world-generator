export type Directions = 'N' | 'S' | 'E' | 'W'

export interface Point {
  x: number
  y: number
}
